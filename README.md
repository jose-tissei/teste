# supply-framework

- - -

   [1]: http://10.2.57.41/app/rest/builds/buildType:(id:SupplyFramework_Master)/statusIcon

   
master | ![TeamCity Build Status][1]
:-----: | :-----
![](https://cdn.iconscout.com/public/images/icon/free/png-32/teamcity-company-brand-logo-3f08e0c5f57f3f5c-32x32.png)  | [**http://10.2.57.41**](http://10.2.57.41)
![](https://secure.gravatar.com/avatar/60929cf718b4dd6de66be7739322ab48?s=32&r=g&d=retro)  | [**http://90.0.0.140:5555/nuget**](http://90.0.0.140:5555/nuget)
 | 

v 1.0.0:

 - Application bootstrapper
 - Generic repository
 - DI with Simple Injector provider
 - Command, CommandHandler for CQRS
 - Domain Notification
 - Asp .Net Core 2.0 support
 - EF Core 2.0 support
 - Fluent Validation extensions